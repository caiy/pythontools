import argparse
import numpy as np
import pandas as pd
import json
import uproot
import warnings

JsonFile = "/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/MiniAnalysis/python/scripts/merge.json"
with open(JsonFile) as f:
    LinkDB = json.load(f)
    NnormDB = LinkDB["Nnorm"]
    variationDB = LinkDB["link"]

variation_name = []
region_name = ''
sample_gen = ''

GenDBfile = "/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/MiniAnalysis/python/scripts/generatorDB.json"
with open(GenDBfile) as f:
    GenDB = json.load(f)


# get DataFrame
def initPD(rootpath, rootname, treename, leafname=[]):
    with uproot.open(rootpath + rootname) as root_file:
        _var_pandas = root_file[treename].pandas.df(leafname)
    return _var_pandas


# get Variation weight by generator name
def getVarWeightByGen(PDMap):

    runNumber = PDMap['mergedRunNumber']
    mcChannel = PDMap['mcChannelNumber']
    inRegion = int(PDMap[region_name])
    weight = PDMap['Weight_mc']
    #print(variation_name, inRegion, mcChannel, runNumber, weight)
    branchName = "GenWeight_LHE_" + variation_name
    varGenWeight = PDMap[branchName]
    nomGenWeight = PDMap['GenWeight']

    MCcompaign = ""
    if runNumber >= 348885:
        MCcompaign = "18"
    elif runNumber >= 325713:
        MCcompaign = "17"
    else:
        MCcompaign = "16"

    if len(variationDB[sample_gen]) == 1:
        ProcessID = '0'
        warnings.warn(sample_gen + " only has Nominal_Inclusive")
    else:
        #for i in variation_name:
        #    if i in variationDB[sample_gen]:
        #        ProcessID = variationDB[sample_gen][i]
        #        break
        if variation_name in variationDB[sample_gen]:
            ProcessID = variationDB[sample_gen][variation_name]
        else:
            raise KeyError(sample_gen + " does not have any matched variation : " + variation_name)

    nomSumWeight = NnormDB["0"][str(mcChannel)][MCcompaign]
    varSumWeight = NnormDB[ProcessID][str(mcChannel)][MCcompaign]
    return inRegion * weight * (nomSumWeight / varSumWeight) * (varGenWeight / nomGenWeight)


# get Variation weight by mcChannel number
def getVarWeightByDSID(PDMap):

    runNumber = PDMap['mergedRunNumber']
    mcChannel = PDMap['mcChannelNumber']
    inRegion = int(PDMap[region_name])
    weight = PDMap['Weight_mc']
    #print(variation_name, inRegion, mcChannel, runNumber, weight)
    branchName = "GenWeight_LHE_" + variation_name
    varGenWeight = PDMap[branchName]
    nomGenWeight = PDMap['GenWeight']

    MCcompaign = ""
    if runNumber >= 348885:
        MCcompaign = "18"
    elif runNumber >= 325713:
        MCcompaign = "17"
    else:
        MCcompaign = "16"

    if len(variationDB[str(mcChannel)]) == 1:
        ProcessID = '0'
        warnings.warn(str(mcChannel) + " only has Nominal_Inclusive")
    else:
        #for i in variation_name:
        #    if i in variationDB[str(mcChannel)]:
        #        ProcessID = variationDB[str(mcChannel)][i]
        #        break
        if variation_name in variationDB[str(mcChannel)]:
            ProcessID = variationDB[str(mcChannel)][variation_name]
        else:
            raise KeyError(str(mcChannel) + " does not have any matched variation : " + variation_name)

    nomSumWeight = NnormDB["0"][str(mcChannel)][MCcompaign]
    varSumWeight = NnormDB[ProcessID][str(mcChannel)][MCcompaign]
    #if inRegion * (varSumWeight-nomSumWeight/nomSumWeight) > 0.1 :
    #    print(str(mcChannel),ProcessID,nomSumWeight,varSumWeight)
    return inRegion * weight * (nomSumWeight / varSumWeight) * (varGenWeight / nomGenWeight)


# get Nominal weight in One region_name
def getNomWeight(PDMap):
    inRegion = int(PDMap[region_name])
    weight = PDMap['Weight_mc']
    return inRegion * weight


# genDict return str to find sumWeight
# GenDB return variation Name
def controlDic(sampleName, varName):
    global variation_name, region_name, sample_gen
    genDict = {
        'W': 'Sherpa221',
        'Z': 'Sherpa221',
        'MultiBoson': 'Sherpa',
        'ttbar': 'PowHegPy8',
        'ttV': 'MG5',
        'ttX': 'DSID',
        'SingleTop': 'DSID',
        'VBFHiggs': 'VBFHiggs',
        'VHiggs': 'VHiggs',
        'ggHiggs': 'ggHiggs',
        'ttHiggs': 'ttHiggs'
    }
    _genDict = {
        'W': 'Sherpa',
        'Z': 'Sherpa',
        'MultiBoson': 'Sherpa',
        'ttbar': 'PowHegPy8_ttbar',
        'ttV': 'MG5_aMCatNLO_Py8',
        'ttX': 'MG5Py8',
        'SingleTop': 'PowHegPy8_SingleTop',
        'VBFHiggs': 'VBFHiggs',
        'VHiggs': 'VHiggs',
        'ggHiggs': 'ggHiggs',
        'ttHiggs': 'ttHiggs'
    }
    genName = _genDict[sampleName]
    return genDict[sampleName], GenDB[genName][varName]


def main(rootFileName, branchName, regionName, sampleName, varName):

    global variation_name, region_name, sample_gen

    region_name = regionName

    branchList = [region_name, "mcChannelNumber", "mergedRunNumber", "Weight_mc", "GenWeight*"]
    PDMap = initPD("/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/TheoreticalUncer/InternalWeight/ISRISF/Ntuple/", rootFileName, branchName,
                   branchList)
    sample_gen, variation_name = controlDic(sampleName, varName)

    sumRawEvent = PDMap[region_name].sum()
    PDMap['nomWeight'] = PDMap.apply(getNomWeight, axis=1)

    if sumRawEvent == 0:
        print("Raw  events = 0 in Region " + region_name)
    else:
        if sample_gen == "DSID":
            PDMap['varWeight'] = PDMap.apply(getVarWeightByDSID, axis=1)
        else:
            PDMap['varWeight'] = PDMap.apply(getVarWeightByGen, axis=1)
        sumNomWeight = PDMap['nomWeight'].sum()
        sumVarWeight = PDMap['varWeight'].sum()
        print(sumRawEvent, sumNomWeight, sumVarWeight)


if __name__ == "__main__":
    main(rootFileName="All.root", branchName="W_Staus_Nominal", regionName="isSR1", sampleName="W", varName="05_05")
    print('All things are done.')

#mc16e if(( mergedRunNumber >= 348885 )){}
#mc16d else if( mergedRunNumber >= 325713 && mergedRunNumber <= 340453 ){}
#mc16a else if( mergedRunNumber <= 311481){}
