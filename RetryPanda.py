import re
import schedule
import time
from pandatools import panda_api

c = panda_api.get_api()


def print_task():
    tasks = c.get_tasks()
    for task in tasks:
        print('taskID={} status={} name={}'.format(task['jeditaskid'], task['status'], task['taskname']))


def get_task_id(task_status, pattern='.*'):
    id_list = []
    name_list = []
    r_pattern = re.compile(pattern)
    tasks = c.get_tasks(status=task_status)
    for task in tasks:
        task_name_str = task['taskname'].encode('utf-8')
        if re.findall(r_pattern, task_name_str):
            id_list.append(task['jeditaskid'])
            name_list.append(task_name_str)
    return id_list, name_list


def retry_job(job_id_list, new_parameters=None):
    if not job_id_list:
        return 0
    if isinstance(job_id_list, list):
        for job_id in job_id_list:
            print('Start Retry ' + str(job_id))
            retry_job(job_id, new_parameters)
    elif isinstance(job_id_list, int):
        communication_status, o = c.retry_task(job_id_list, new_parameters)
        if communication_status:
            server_return_code, dialog_message = o
            if o == 0:
                print('OK')
            else:
                print("Not good with {} : {}".format(server_return_code, dialog_message))
    else:
        print(job_id_list, " is not a List or Int, do nothing")
        return 0


"""Define the job which will be looping"""


def loop_retry_jobs():
    print(time.ctime(time.time()))
    #lists,names = get_task_id('failed','.*')
    #print(names)
    #retry_job(lists, new_parameters={'nFilesPerJob':'1'})
    lists, names = get_task_id('finished', '.*')
    print(names)
    retry_job(lists, new_parameters={'nFilesPerJob': '1'})


if __name__ == "__main__":
    #lists,names = get_task_id('failed','\.C1.*v02_part.\.100.*truth3')
    lists, names = get_task_id('failed|finished|running|broken', '.*')
    print(names)
    #retry_job(lists )
    #retry_job(lists, new_parameters={'nFilesPerJob':'1', 'excludedSite':['UAM_LCG2','TW_FTT']})
    #a=c.show_tasks( task_ids = None, limit=1000, days=14, status='failed')

    #loop_retry_jobs()
    #schedule.every().hour.do(loop_retry_jobs)
    #while True:
    #    schedule.run_pending()
