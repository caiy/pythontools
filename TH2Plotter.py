import uproot
import numpy as np
import matplotlib.pyplot as plt


def get_TH2F_from_root(root_file_name, tree_name, branch_x_name, branch_y_name, branch_weight_name, x_edges, y_edges):
    root_file = uproot.open(root_file_name)[tree_name]
    #print(root_file[branch_x_name])
    #print(root_file[branch_y_name])
    #return np.histogram2d(root_file[branch_x_name].array(), root_file[branch_y_name].array(), bins=[20,25] ,range=[[0,200], [0,500]] ,weights=root_file[branch_weight_name].array())
    return root_file[branch_x_name].array(), root_file[branch_y_name].array(), root_file[branch_weight_name].array()


input_path = "/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis/QCD/run/"
file_name = "Zll.root"

xe = np.array([range(0, 200, 10)])
ye = np.array([range(0, 500, 20)])
#H,xe,ye = get_TH2F_from_root(input_path+file_name, "NOMINAL", "rTauPt", "rMET", "Weight_mc", xe, ye)
#X, Y = np.meshgrid(xe, ye)
#plt.pcolormesh(X, Y, H)

sct_x, sct_y, sct_W = get_TH2F_from_root(input_path + file_name, "NOMINAL", "rTauPt", "rMET", "Weight_mc", xe, ye)
fig = plt.figure()
plt.hist2d(sct_x, sct_y, weights=sct_W, norm=[0, 1])
plt.savefig('yes.png')
