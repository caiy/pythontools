#Systematic("ZjetsRenormTheo", configMgr.weights,1.+0.232,1.-0.155 , "user","userOverallSys")#9.11

import os
import csv
import numpy


def str_abs(i):
    f_i = float(i)
    f_i = round(100 * abs(f_i), 1)
    i = str(f_i)
    return i


input_path = 'C1C1_C1N2_OS_Theoretical_Uncertainties_Sum.CSV'
bkg_name_start_end = [['Higgs', 2, 7], ['MultiBoson', 10, 17], ['Top', 20, 31], ['Z', 34, 41], ['W', 44, 51]]

reader = csv.reader(open(input_path, "rb"), delimiter=",")
sys_list = list(reader)

total_region_num = len(sys_list[0])

for name_start_end in bkg_name_start_end:
    bkg_name = name_start_end[0]
    start_step = name_start_end[1]
    end_step = name_start_end[2]
    print('    ' + bkg_name + '    ')

    first_line = "Region "
    l_num = "l"
    for line_num in range(start_step, end_step, 2):
        first_line = first_line + ' &  $\\sigma_{' + sys_list[line_num][0] + '}$ '
        l_num = l_num + 'l'
    first_line = first_line + '\\\\'
    print('\\begin{tabular}{' + l_num + '}')
    print('\\toprule')
    print(first_line)
    print('\\hline')

    for col_num in range(1, total_region_num):
        region_name = sys_list[0][col_num]
        #print( '    addBkgTheoUncert('+bkg_name+'Theo, "'+bkg_name+'_Staus'+'", "'+region_name+'", '+region_name+'set) ' )
        second_line = '\multirow{2}{*}{' + region_name + '} '
        third_line = "                     "
        for line_num in range(start_step, end_step, 2):
            var_up = str_abs(sys_list[line_num][col_num])
            var_dw = str_abs(sys_list[line_num + 1][col_num])
            second_line = second_line + ' &   $ +' + var_up + '\\% $ '
            third_line = third_line + ' &   $ -' + var_dw + '\\% $ '
        second_line = second_line + ' \\\\'
        third_line = third_line + ' \\\\'
        print(second_line)
        print(third_line)
        print('\\hline')

        #for col_num in range(1,total_region_num) :
        #
        #    region_name = sys_list[0][col_num]
        #
        #    var_up = str_abs(sys_list[line_num][col_num])
        #    var_dw = str_abs(sys_list[line_num+1][col_num])
        #
        #    print( '    '+bkg_name+'Theo["'+bkg_name+var_name+"_"+region_name+'"] ' + '= Systematic( "'+bkg_name+"_"+var_name+'_Theo", configMgr.weights, (1.+'+var_up+'), (1-'+var_dw+'), "user", "userOverallSys" )' )
