import glob
import numpy as np
import re
import json

folderPattern = "user.caiy.sample.mc15_13TeV.*"
txtPattern = "/*.XYZ.txt"
"""Start"""
out_json = {}
sum_weight_json = {}
event_num_json = {}
acc_json = {}

folder_list = glob.glob("./" + folderPattern)  # all folder with ${folderPattern}

for each_folder in folder_list:
    txt_list = []
    txt_list = glob.glob(each_folder + txtPattern)  # all txt with ${txtPattern} in each foldeer
    first_file = 0
    SumWeight = 0
    EventsNum = 0
    Acceptance = 0

    if txt_list:
        first_file = 1
        for each_txt in txt_list:
            txt_array = np.loadtxt(each_txt, skiprows=1, usecols=(1, 2), delimiter=',')
            #if first_file:
            #    Acceptance = txt_array[1][1]
            #else:
            #    Acceptance = ( txt_array[1][1] * txt_array[0][1] + Acceptance * SumWeight ) / ( txt_array[0][1] + SumWeight )
            SumWeight += txt_array[0][1]
            EventsNum += txt_array[0][0]
            Acceptance += txt_array[0][1] * txt_array[1][1]

    if SumWeight != 0:
        Acceptance = Acceptance / SumWeight

    DSID = re.findall(r'[0-9]{6}', each_folder)[0]  # find DSID by folder name
    sum_weight_json[str(DSID)] = {"16": SumWeight}
    event_num_json[str(DSID)] = EventsNum
    acc_json[str(DSID)] = Acceptance
    #print(DSID)
"""End search"""

out_json["Norm"] = sum_weight_json
out_json["EventNumber"] = event_num_json
out_json["Acceptance"] = acc_json
with open('yields.json', 'w') as outfile:
    json.dump(out_json, outfile, indent=2)
