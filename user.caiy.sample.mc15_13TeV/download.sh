#prun --exec 'simpleAnalysis -n -s layout=run2 -a EwkDiTauPre %IN' --outputs='*.txt,*.root' --useRootCore --inDS mc15_13TeV.407356.PhH7EG_H7UE_ttbarHT6c_1k_hdamp258p75_704_nonAH.deriv.DAOD_TRUTH3.e6894_p3655 --outDS user.caiy.mc15_13TeV.407356.PhH7EG_H7UE_ttbarHT6c_1k_hdamp258p75_704_nonAH

#mc15_13TeV.407356.PhH7EG_H7UE_ttbarHT6c_1k_hdamp258p75_704_nonAH.deriv.DAOD_TRUTH3.e6894_p3655

#prun --osMatching --exec 'source x86_64-centos7-gcc8-opt/setup.sh;simpleAnalysis -a EwkDiTauPre %IN' --outputs='*.txt,*.root' --extFile \*.root --athenaTag 21.2.100,AnalysisBase --maxFileSize 40000000 --noCompile --followLinks --inDS 'mc15_13TeV.366036.Sherpa_222_NNPDF30NNLO_lllljj_EW6_QSF4.deriv.DAOD_TRUTH3.e7360_p3655' --outDS user.caiy.TestSampleForSimpleAnalysis3
threadNum=8
total=""
input="./result.txt"
exec 4<$input
while read -u4 line
do
	var=$(cut -c-10 <<< "$line")
	if [ "$var" == 'mc15_13TeV' ]
	then
		lineR=${line% *}
		tag=${line%.deriv.*}
		tagfull="user.caiy.sample.fixQCD.""$tag"
		total="$total ""$tagfull"
		#echo "user.caiy.sample.""$tag"
		#prun --exec 'simpleAnalysis -n -s layout=run2 -a EwkDiTauPre %IN' --outputs='*.txt,*.root' --useRootCore --inDS $lineR --outDS user.caiy.sample."$tag"

	fi
done
#Closed
echo $total > test.log
#nohup ./msource "downloadSingle.sh" $threadNum $total > log.txt 2>&1 &

