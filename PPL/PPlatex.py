import os
import argparse


def line_prepender(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)


def fixedipd(filename):
    with open(filename, 'r') as file:
        filedata = file.read()
    filedata = filedata.replace(r'\,\ipb', r'$fb^{-1}$')
    with open('temp.tex', 'w') as file:
        file.write(filedata)


parser = argparse.ArgumentParser(description='latex files')
parser.add_argument('-i', '--input', action='store', help='input file name')
parser.add_argument('-o', '--output', default="temp.tex", action='store', help='output 1 file ! w+ !')
args = vars(parser.parse_args())

cwd = os.getcwd()
inputlatex = args['input']

document=[r"\documentclass{article}           ",\
r"\usepackage{booktabs}                       ",\
r"\usepackage{amsmath}                        ",\
r"\usepackage{multirow}                       ",\
r"\usepackage{multicol}                       ",\
r"\usepackage{graphicx}                       ",\
r"\usepackage{geometry}                       ",\
r"\geometry{legalpaper, landscape, margin=2in}",\
r"\begin{document}"]
end = "\end{document}"

fixedipd(inputlatex)

for i in reversed(document):
    line_prepender("temp.tex", i)

with open("temp.tex", 'a') as file:
    file.write('\n')
    file.write(end)

os.system("pdflatex temp.tex")
