#-*- coding: UTF-8 -*-
import re
import sys
import os
import argparse

parser = argparse.ArgumentParser(description='diff 2 files')
parser.add_argument('input', nargs='+', action='store', help='input 2 file')
parser.add_argument('-o', '--output', action='store', help='output 1 file ! w+ !')
args = vars(parser.parse_args())

input_split = args['input']

str1 = []
str2 = []
str_dump = []

fa = open(input_split[0], 'r')
fb = open(input_split[1], 'r')
fc = open(args['output'], 'w+')

for line in fa.readlines():
    str1.append(line.replace("\n", ''))

for line in fb.readlines():
    str2.append(line.replace("\n", ''))

for i in str1:
    if i in str2:
        str_dump.append(i)

str_all = set(str1 + str2)

for i in str_dump:
    if i in str_all:
        str_all.remove(i)
for i in list(str_all):
    fc.write(i + '\n')

fa.close()
fb.close()
fc.close()
