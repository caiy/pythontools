import datetime
import threading


def MonitorSystem():
    now = datetime.datetime.now()
    print(now)


def loopMonitor(inc):
    MonitorSystem()
    t = threading.Timer(inc, loopMonitor, (inc,))
    t.start()


loopMonitor(5)
