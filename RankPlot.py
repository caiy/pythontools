import numpy as np
import matplotlib.pyplot as plt


def CalSys(file_path, file_head):
    head_name = file_path + file_head
    Sys_Alpha_Map = {}
    NominalName = "Nominal"
    SysList = [
        "pileup_UP", "pileup_DOWN", "jet_UP", "jet_DOWN", "tau_ID_UP", "tau_ID_DOWN", "tau_Reco_UP", "tau_Reco_DOWN", "tau_eVeto_UP",
        "tau_eVeto_DOWN", "lepCombine_UP", "lepCombine_DOWN", "lumi_UP", "lumi_DOWN"
    ]

    InputFileList = [NominalName] + SysList

    for Sys in InputFileList:

        with open(head_name + Sys + ".txt", "r") as infile:
            #print(infile.readline())
            Sys_Alpha_Map[Sys] = float(infile.readline().split()[1])
            #print(Sys_Alpha_Map[Sys].split())

    Sys_DeltaAlpha_Map = {}
    alphaSys_value = 0

    for Sys in SysList:
        if Sys.find("UP") != -1:
            down_name = Sys.replace("UP", "DOWN")
            sys_up_value = Sys_Alpha_Map[Sys]
            sys_dw_value = Sys_Alpha_Map[down_name]
            sigma_up = (sys_up_value - Sys_Alpha_Map[NominalName]) / Sys_Alpha_Map[NominalName]
            sigma_dw = (sys_dw_value - Sys_Alpha_Map[NominalName]) / Sys_Alpha_Map[NominalName]
            #print(sigma_up,sigma_dw)
            subSysName = Sys[0:-3]
            if abs(sigma_up) > abs(sigma_dw):
                Sys_DeltaAlpha_Map[subSysName] = sigma_up
                alphaSys_value = np.sqrt(alphaSys_value * alphaSys_value + sigma_up * sigma_up)
            else:
                Sys_DeltaAlpha_Map[subSysName] = sigma_dw
                alphaSys_value = np.sqrt(alphaSys_value * alphaSys_value + sigma_dw * sigma_dw)

    print(Sys_DeltaAlpha_Map, alphaSys_value)
    """ Begin plotting """
    xticks = []
    delta_alpha = []
    delta_alpha_err = []
    sorted_Sys_keys = sorted(Sys_DeltaAlpha_Map, key=lambda dict_key: abs(Sys_DeltaAlpha_Map[dict_key]), reverse=True)
    #print(sorted_Sys_keys)

    for key in sorted_Sys_keys:
        xticks.append(key)
        delta_alpha.append(Sys_DeltaAlpha_Map[key] * Sys_Alpha_Map[NominalName])
        delta_alpha_err.append(abs(Sys_DeltaAlpha_Map[key]))

    fig, ax = plt.subplots(1, 1)
    ax.bar(range(len(xticks)), delta_alpha, align='center')
    ax.set_xticks(range(len(xticks)))
    ax.set_xticklabels(xticks, fontsize=12)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    ax.text(len(xticks) - 3.5, 0.8, r"$\alpha_{nominal}=$" + str(round(Sys_Alpha_Map[NominalName], 2)) + "%", fontsize=14)
    ax.text(len(xticks) - 3.5, 0.67, r"total relative $\sigma_{\alpha}=$" + str(round(100 * alphaSys_value, 1)) + "%", fontsize=12)
    ax.text(len(xticks) - 3.5, 0.55, "rel: xx%", fontsize=12, color='g')

    ax.text(-0.6, 0.83, r'ATLAS', fontsize=16, weight="bold", style="italic")
    ax.text(0.65, 0.83, "Internal", fontsize=16)

    ax.plot([-0.5, len(delta_alpha) - 0.5], [0, 0], linewidth=1, color='0.8', linestyle=(0, (1, 1)))
    for i in range(len(delta_alpha)):
        x_pos = i - 0.3
        if delta_alpha[i] != 0:
            y_pos = delta_alpha[i] + 0.06 * delta_alpha[i] / abs(delta_alpha[i]) - 0.03
        else:
            y_pos = delta_alpha[i] + 0.06 - 0.03
        ax.text(x_pos, y_pos, str(round(100 * delta_alpha_err[i], 1)) + "%", color='g')

    ax.set_ylim([-1, 1])
    ax.set_ylabel(r"$\Delta\alpha$[%]", fontsize=14)
    ax.set_xlabel("Systematic", fontsize=14)

    fig.tight_layout()
    fig.savefig(file_head + ".png")
    fig.savefig(file_head + ".eps")


CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run/", "prong_1_")
CalSys("/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/TES/thread/run_3p/", "prong_3_")
