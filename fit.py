import pyhf

import json
import numpy as np
import glob
import regex as re
import matplotlib.pyplot as plt

additional_json = {
    "covqual": 3,
    "dodgycov": 0,
    "excludedXsec": -999007,
    "expectedUpperLimit": -1,
    "expectedUpperLimitMinus1Sig": -1,
    "expectedUpperLimitMinus2Sig": -1,
    "expectedUpperLimitPlus1Sig": -1,
    "expectedUpperLimitPlus2Sig": -1,
    "fID": -1,
    "failedcov": 0,
    "failedfit": 0,
    "failedp0": 0,
    "failedstatus": 0,
    "fitstatus": 0,
    "mode": -1,
    "nexp": -1,
    "nofit": 0,
    "p0": 0,
    "p0d1s": -1,
    "p0d2s": -1,
    "p0exp": -1,
    "p0u1s": -1,
    "p0u2s": -1,
    "p1": 0,
    "seed": 0,
    "sigma0": -1,
    "sigma1": -1,
    "upperLimit": -1,
    "upperLimitEstimatedError": -1,
    "xsec": -999007,
}


def fit(file_path):

    wspace = pyhf.Workspace(json.load(open(file_path))  #"../C1C1_json/Fit_C1C1SR0_C1C1_1100.0_600.0.json"
                           )

    wspace = pyhf.Workspace(json.load(open(file_path)))

    model = wspace.model(
        modifier_settings={
            'normsys': {
                'interpcode': 'code4'
            },
            'histosys': {
                'interpcode': 'code4p'
            },
        },)

    data = wspace.data(model)

    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, return_expected_set=True)

    cls_exp = {}
    cls_obs = {'CLs': CLs_obs.tolist()}
    for expected_value, n_sigma in zip(CLs_exp, np.arange(-2, 3)):
        if n_sigma < 0:
            cls_exp[f"clsd{abs(n_sigma):d}s"] = expected_value.tolist()
        elif n_sigma == 0:
            cls_exp[f"CLsexp"] = expected_value.tolist()
        else:
            cls_exp[f"clsu{n_sigma:d}s"] = expected_value.tolist()
    return {**cls_obs, **cls_exp, **additional_json}


def apply_fit(data, dict_harvest=[]):
    filename = data['filename']
    region = data['region']
    m = re.compile(r"(\d+)\.*(\d+)_(\d+)\.*(\d+)").search(filename).group(0)
    outname = f'results/region_{region}_point_{m}.json'
    print(" * start" + filename)
    try:
        d = fit(filename)
        x = m.split("_")
        d["mx"] = float(x[0])
        d["my"] = float(x[1])
        json.dump(d, open(outname, 'w'), indent=4)
        dict_harvest.append(d)
    except:
        return 0


cA = [{'region': 'C1N2', 'filename': f} for f in glob.glob('../C1N2_json/Fit_C1N2SR0_C1N2_*.json')]

harvest = []

for i in cA:
    apply_fit(i, harvest)
    print(harvest)

json.dump(harvest, open("Nominal_C1N2.json", 'w'), indent=4)
"""
for BKG only fit, need set POI to none in json file.
"""
#result = pyhf.infer.mle.fit(
#    data,
#    model,
#    return_uncertainties=True
#)
#bestfit, errors = result.T
#
#pulls = pyhf.tensorlib.concatenate(
#    [
#        (bestfit[model.config.par_slice(k)] - model.config.param_set(k).suggested_init)
#        / model.config.param_set(k).width()
#        for k in model.config.par_order
#            if model.config.param_set(k).constrained
#    ]
#)
#
#pullerr = pyhf.tensorlib.concatenate(
#    [
#        errors[model.config.par_slice(k)] / model.config.param_set(k).width()
#        for k in model.config.par_order
#        if model.config.param_set(k).constrained
#    ]
#)
#
#labels = np.asarray(
#    [
#        "{}[{}]".format(k, i) if model.config.param_set(k).n_parameters > 1 else k
#        for k in model.config.par_order
#        if model.config.param_set(k).constrained
#        for i in range(model.config.param_set(k).n_parameters)
#    ]
#)
#
#_order = np.argsort(labels)
#bestfit = bestfit[_order]
#errors = errors[_order]
#labels = labels[_order]
#pulls = pulls[_order]
#pullerr = pullerr[_order]
#
#fig, ax = plt.subplots()
#fig.set_size_inches(20, 5)
#
## set up axes labeling, ranges, etc...
#ax.set_xticklabels(labels, rotation=90)
#ax.set_xlim(-0.5, len(pulls) - 0.5)
#ax.set_title("Pull Plot", fontsize=18)
#ax.set_ylabel(r"$(\theta - \hat{\theta})\,/ \Delta \theta$", fontsize=18)
#
## let's make the +/- 2.0 horizontal lines
#ax.hlines([-2, 2], -0.5, len(pulls) - 0.5, colors="k", linestyles="dotted")
## let's make the +/- 1.0 horizontal lines
#ax.hlines([-1, 1], -0.5, len(pulls) - 0.5, colors="k", linestyles="dashdot")
## let's make the +/- 2.0 sigma band
#ax.fill_between([-0.5, len(pulls) - 0.5], [-2, -2], [2, 2], facecolor="yellow")
## let's make the +/- 1.0 sigma band
#ax.fill_between([-0.5, len(pulls) - 0.5], [-1, -1], [1, 1], facecolor="green")
## let's draw a horizontal line at pull=0.0
#ax.hlines([0], -0.5, len(pulls) - 0.5, colors="k", linestyles="dashed")
## finally draw the pulls
#ax.scatter(range(len(pulls)), pulls, c="k")
## and their errors
#ax.errorbar(
#    range(len(pulls)), pulls, c="k", xerr=0, yerr=pullerr, marker=".", fmt="none"
#);
