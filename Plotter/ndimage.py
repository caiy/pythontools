# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import csv
from scipy.interpolate import griddata
#from matplotlib import mlab as ml

label = r'highMass-OS2, CR-A'
xAxis = r'$m{\tilde{\chi}^{\pm}_1}$ / $m{\tilde{\chi}^{0}_2}$ [GeV]'
#xAxis=r'$m{\tilde{\chi}^{\pm}_1}$ [GeV]'
yAxis = r'$m{\tilde{\chi}^1_0}$ [GeV]'
Ndata = 17

xx = []
yy = []
zz = []
xxx = []
yyy = []

with open('./C1N2high.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        xx.append(float(row[0]))
        yy.append(float(row[1]))
        zz.append(100 * float(row[2]) / Ndata)
xx = np.asarray(xx)
yy = np.asarray(yy)
zz = np.asarray(zz)

x = np.linspace(0, 1300, 130)
y = np.linspace(0, 1000, 100)
line = np.linspace(0, 500, 50)
X, Y = np.meshgrid(x, y)
z = griddata(points=(xx, yy), values=zz, xi=(X, Y), method='linear')

#levels=[0.64,2.64,5]
#levels2=[0.64,1.64,2.64,10]

#color bar
fig = plt.contourf(x, y, z, vmin=0., vmax=100., cmap=cm.coolwarm)
m = plt.cm.ScalarMappable(cmap=cm.coolwarm)
m.set_array(z)
m.set_clim(0., 100.)
cbar = plt.colorbar(m, boundaries=np.linspace(0, 100, 6), format='%.0f%%')
cbar.set_label('contamination')

#grid, forbidden line and text
plt.grid(color='grey', linestyle='--', linewidth=0.5)
plt.plot(line, line, color='grey', linestyle='dashed')
plt.text(200, 300, r'$m{\tilde{\tau}} > m{\chi}^1_0$', rotation=50, color='grey', fontsize=10)
for i in range(len(xx)):
    plt.text(xx[i], yy[i] + 35, str(round(zz[i], 1)) + "%", rotation=20, color='red', fontsize=4)

#ATLAS Internal, lumi, cmEnergy
plt.text(100, 900, r'ATLAS', fontsize=14, weight="bold", style="italic")
plt.text(350, 900, r'Internal', fontsize=14)
plt.text(100, 800, r'$\sqrt{s}=13\ TeV,\ 139\ fb^{-1}$', fontsize=10)

#plot Right head label and axix
plt.text(750, 900, label, fontsize=10)

#
plt.xlabel(xAxis)
plt.ylabel(yAxis)

plt.savefig("ndimage.png", dpi=800)
