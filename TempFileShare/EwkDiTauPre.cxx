#include <string>

#include "SimpleAnalysis/AnalysisClass.h"

DefineAnalysis(EwkDiTauPre)

    // Copy from OneLepton2018
    // Obj definition : https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau/-/blob/master/XAMPPstau/data/SUSYTools_Stau.conf
    // OverlapRemoval : https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/OverlapRemovalTool.cxx#L106
    // Pre-selection : https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau/-/blob/master/XAMPPstau/Root/StauHadHadAnalysisConfig.cxx

    void EwkDiTauPre::Init() {
    // Exclusion SRs
    addRegion("preselection");
}

void EwkDiTauPre::ProcessEvent(AnalysisEvent* event) {
    auto baseEle = event->getElectrons(4.5, 2.47, ELooseBLLH);
    auto baseMuon = event->getMuons(3, 2.70, MuMedium | MuNotCosmic | MuZ05mm | MuQoPSignificance);
    auto allTaus = event->getTaus(20., 2.5, TauOneProng | TauThreeProng);
    auto baseJets = event->getJets(20., 2.8);
    auto met_Vect = event->getMET();
    float met = met_Vect.Pt();
    auto weights = event->getMCWeights();

    AnalysisObjects baseTaus;
    for (const auto& tau : baseTaus) {
        if (fabs(tau.charge()) == 1 && !(fabs(tau.Eta()) > 1.37 && fabs(tau.Eta()) < 1.52)) {
            baseTaus.push_back(tau);
        }
    }

    // overlapRemoval
    auto radiusCalcLep = [](const AnalysisObject& lep, const AnalysisObject&) { return (0.04 + 10 / lep.Pt()) > 0.4 ? 0.4 : 0.04 + 10 / lep.Pt(); };
    //  Athena config file : https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/AnalysisCommon/AssociationUtils/python/config.py
    //  XAMPP config file : https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau/-/blob/master/XAMPPstau/data/SUSYTools_Stau.conf#L110
    // PFlow-Muon-jet OR
    //  not available
    // Electron-electron OR
    //  We do not doEleEleOR
    // Tau-electron OR
    baseTaus = overlapRemoval(baseTaus, baseEle, 0.2);
    // Tau-muon OR
    baseTaus = overlapRemoval(baseTaus, baseMuon, 0.2);
    // Electron-muon OR
    baseEle = overlapRemoval(baseEle, baseMuon, 0.01);
    // Pho-electron OR
    // Pho-muon OR
    // Electron-jet OR
    baseJets = overlapRemoval(baseJets, baseEle, 0.2);
    baseEle = overlapRemoval(baseEle, baseJets, radiusCalcLep); // OR.DoBoostedElectron: true, OR.DoBoostedMuon: true, So we use UseSlidingDR
    // Muon-jet OR
    baseJets = overlapRemoval(baseJets, baseMuon, 0.2, LessThan3Tracks); // expect for the jets has more than 3 ID tracks.
    baseMuon = overlapRemoval(baseMuon, baseJets, radiusCalcLep);        // OR.DoBoostedElectron: true, OR.DoBoostedMuon: true, So we use UseSlidingDR
    // Tau-jet OR
    baseTaus = overlapRemoval(baseTaus, baseJets, 0.2);
    baseJets = overlapRemoval(baseJets, baseTaus, 0.2);
    // Pho-jet OR
    // Electron-fatjet OR
    //  We do not doFatJets, OR.DoFatJets: false
    // jet-fatjet OR
    //  We do not doFatJets, OR.DoFatJets: false

    // Get signal electrons with FCLoose for pT < 200 GeV and FCHighPtCaloOnly for pT > 200  # tight iso required for electrons pt > 200 GeV
    // filterObjects() only allows lower pT cut, so work around that
    auto signalEle = filterObjects(baseEle, 25, 2.47, ETightLH | ED0Sigma5 | EZ05mm | EIsoFCLoose);
    AnalysisObjects signalEleLowPt;
    for (const auto& ele : signalEle) {
        if ((ele.Pt() < 200.)) signalEleLowPt.push_back(ele);
    }
    auto signalEleHighPt = filterObjects(baseEle, 200., 2.47, ETightLH | ED0Sigma5 | EZ05mm | EIsoFCHighPtCaloOnly);
    signalEle = signalEleLowPt + signalEleHighPt;

    // Get signal muons and FCLoose for both low pT and high pT
    auto signalMuon = filterObjects(baseMuon, 25, 2.7, MuD0Sigma3 | MuZ05mm | MuIsoFCLoose);
    auto signalLept = signalEle + signalMuon;
    auto signalTaus = baseTaus;

    auto signalJets = filterObjects(baseJets, 20., 2.80, JVT120Jet | JVTTight);
    auto signalBJets = filterObjects(signalJets, 30., 2.8, BTag77MV2c10);

    unsigned int N_baseEle = baseEle.size();
    unsigned int N_baseMuon = baseMuon.size();
    unsigned int N_baseLept = N_baseEle + N_baseMuon;
    unsigned int N_baseTau = baseTaus.size();
    unsigned int N_signalEle = signalEle.size();
    unsigned int N_signalMuon = signalMuon.size();
    unsigned int N_signalLept = N_signalEle + N_signalMuon;
    unsigned int N_signalTau = signalTaus.size();
    unsigned int N_signalJets = signalJets.size();
    unsigned int N_signalBJets = signalBJets.size();

    // Preselection
    // (>= 1 tau && >= 1 ele && ele1Pt > 24)
    bool pass1e1t = false;
    bool pass1m1t = false;
    bool passAsy = false;
    bool passXe = false;
    if (N_baseTau >= 1 && N_baseEle >= 1) {
        if (baseEle[0].Pt() > 24) pass1e1t = true;
    }
    // (>= 1 tau && >= 1 muon && muon1Pt > 20)
    if (N_baseTau >= 1 && N_baseMuon >= 1) {
        if (baseMuon[0].Pt() > 20) pass1m1t = true;
    }
    // >=2 tau && tau1pT > 35 && tau2pT > 25 && Met > 50
    // >=2 tau && tau1pT > 80 && tau2pT > 50
    if (N_baseTau >= 2) {
        if (baseTaus[0].Pt() > 35 && baseTaus[1].Pt() > 25 && met > 50) passXe = true;
        if (baseTaus[0].Pt() > 80 && baseTaus[1].Pt() > 50) passAsy = true;
    }

    if (!(pass1e1t || pass1m1t || passXe || passAsy)) return;

    // Analysis observables
    // float mt=0, ht=0, meff=0, apl=0, lepApl=0;
    // mt   = calcMT(signalLept[0], met_Vect);
    // ht   = sumObjectsPt(signalJets,999,30);
    // meff = ht + met+ signalLept[0].Pt();
    // apl   = aplanarity(signalJets);
    // lepApl = aplanarity(signalJets + signalLept);
    // double METsig = event->getMETSignificance();

    // ntupVar("AnalysisType", (baseMuon.size() == 1) ? 2 : 1);
    // ntupVar("met",met);
    // ntupVar("meff",meff);
    // ntupVar("ht",ht);
    // ntupVar("apl",apl);
    // ntupVar("lepApl",lepApl);
    // ntupVar("mt",mt);
    // ntupVar("nJet30",(int)signalJets.size());
    // ntupVar("nBJet30_MV2c10",(int)signalBJets.size());
    // ntupVar("lep1Pt", signalLept[0].Pt());
    // ntupVar("lep1Eta", signalLept[0].Eta());
    // ntupVar("lep1Phi", signalLept[0].Phi());
    // ntupVar("jet1Pt", signalJets[0].Pt());
    // ntupVar("jet1Eta", signalJets[0].Eta());
    // ntupVar("jet1Phi", signalJets[0].Phi());
    // ntupVar("nLep_signal", (int)signalLept.size());
    // ntupVar("nLep_base", (int)N_baseLept);

    // new Branch
    ntupVar("mcChannelNumber", event->getMCNumber());
    ntupVar("Weight_mc", event->getMCWeights());
    // ntupVar("ColinearMTauTau", &ColinearMTauTau, &b_ColinearMTauTau);
    // ntupVar("CosChi1", &CosChi1, &b_CosChi1);
    // ntupVar("CosChi2", &CosChi2, &b_CosChi2);
    ntupVar("GenFiltHT", event->getGenHT());
    ntupVar("GenFiltMET", event->getGenMET());
    // ntupVar("GenWeight", &GenWeight, &b_GenWeight);
    // ntupVar("Ht_Jet", &Ht_Jet, &b_Ht_Jet);
    // ntupVar("Ht_Lep", &Ht_Lep, &b_Ht_Lep);
    // ntupVar("Ht_Tau", &Ht_Tau, &b_Ht_Tau);
    // ntupVar("LQ_ncl", &LQ_ncl, &b_LQ_ncl);
    // ntupVar("MCT", &MCT, &b_MCT);
    // ntupVar("MET_BiSect", &MET_BiSect, &b_MET_BiSect);
    // ntupVar("MET_Centrality", &MET_Centrality, &b_MET_Centrality);
    // ntupVar("MET_CosMinDeltaPhi", &MET_CosMinDeltaPhi, &b_MET_CosMinDeltaPhi);
    // ntupVar("MET_LepTau_DeltaPhi", &MET_LepTau_DeltaPhi, &b_MET_LepTau_DeltaPhi);
    // ntupVar("MET_LepTau_LeadingJet_VecSumPt", &MET_LepTau_LeadingJet_VecSumPt, &b_MET_LepTau_LeadingJet_VecSumPt);
    // ntupVar("MET_LepTau_VecSumPhi", &MET_LepTau_VecSumPhi, &b_MET_LepTau_VecSumPhi);
    // ntupVar("MET_LepTau_VecSumPt", &MET_LepTau_VecSumPt, &b_MET_LepTau_VecSumPt);
    // ntupVar("MET_SumCosDeltaPhi", &MET_SumCosDeltaPhi, &b_MET_SumCosDeltaPhi);
    // ntupVar("MT2_max", &MT2_max, &b_MT2_max);
    // ntupVar("MT2_min", &MT2_min, &b_MT2_min);
    // ntupVar("Meff", &Meff, &b_Meff);
    // ntupVar("Meff_TauTau", &Meff_TauTau, &b_Meff_TauTau);
    ntupVar("MetTST_met", met_Vect.Pt());
    ntupVar("MetTST_phi", met_Vect.Phi());
    // ntupVar("MetTST_sumet", &MetTST_sumet, &b_MetTST_sumet);
    // ntupVar("PTt", &PTt, &b_PTt);
    // ntupVar("RandomLumiBlockNumber", &RandomLumiBlockNumber, &b_RandomLumiBlockNumber);
    // ntupVar("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
    ntupVar("SUSYFinalState", event->getSUSYChannel());
    // ntupVar("TruthMET_met", &TruthMET_met, &b_TruthMET_met);
    // ntupVar("TruthMET_phi", &TruthMET_phi, &b_TruthMET_phi);
    // ntupVar("TruthMET_sumet", &TruthMET_sumet, &b_TruthMET_sumet);
    // ntupVar("VecSumPt_LepTau", &VecSumPt_LepTau, &b_VecSumPt_LepTau);
    // ntupVar("Vtx_n", &Vtx_n, &b_Vtx_n);
    // ntupVar("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
    // ntupVar("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
    // ntupVar("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
    // ntupVar("bcid", &bcid, &b_bcid);
    // ntupVar("coreFlags", &coreFlags, &b_coreFlags);
    // ntupVar("corr_avgIntPerX", &corr_avgIntPerX, &b_corr_avgIntPerX);
    // ntupVar("corr_avgIntPerX_PRW_DATASF__1down", &corr_avgIntPerX_PRW_DATASF__1down, &b_corr_avgIntPerX_PRW_DATASF__1down);
    // ntupVar("corr_avgIntPerX_PRW_DATASF__1up", &corr_avgIntPerX_PRW_DATASF__1up, &b_corr_avgIntPerX_PRW_DATASF__1up);
    ntupVar("electrons", signalEle, true, true, false, true);
    // ntupVar("electrons_IFFClassType", &electrons_IFFClassType, &b_electrons_IFFClassType);
    // ntupVar("electrons_MT", &electrons_MT, &b_electrons_MT);
    // ntupVar("electrons_charge", &electrons_charge, &b_electrons_charge);
    // ntupVar("electrons_d0sig", &electrons_d0sig, &b_electrons_d0sig);
    // ntupVar("electrons_e", &electrons_e, &b_electrons_e);
    // ntupVar("electrons_eta", &electrons_eta, &b_electrons_eta);
    // ntupVar("electrons_isol", &electrons_isol, &b_electrons_isol);
    // ntupVar("electrons_phi", &electrons_phi, &b_electrons_phi);
    // ntupVar("electrons_pt", &electrons_pt, &b_electrons_pt);
    // ntupVar("electrons_signal", &electrons_signal, &b_electrons_signal);
    // ntupVar("electrons_truthOrigin", &electrons_truthOrigin, &b_electrons_truthOrigin);
    // ntupVar("electrons_truthType", &electrons_truthType, &b_electrons_truthType);
    // ntupVar("electrons_z0sinTheta", &electrons_z0sinTheta, &b_electrons_z0sinTheta);
    // ntupVar("emt_MT2_max", &emt_MT2_max, &b_emt_MT2_max);
    // ntupVar("emt_MT2_min", &emt_MT2_min, &b_emt_MT2_min);
    // ntupVar("eventNumber", &eventNumber, &b_eventNumber);
    // ntupVar("forwardDetFlags", &forwardDetFlags, &b_forwardDetFlags);
    ntupVar("jets", signalJets, true, false, false, true);
    // ntupVar("jets_BTagScore", &jets_BTagScore, &b_jets_BTagScore);
    // ntupVar("jets_Jvt", &jets_Jvt, &b_jets_Jvt);
    // ntupVar("jets_NTrks", &jets_NTrks, &b_jets_NTrks);
    // ntupVar("jets_bjet", &jets_bjet, &b_jets_bjet);
    // ntupVar("jets_eta", &jets_eta, &b_jets_eta);
    // ntupVar("jets_isBadTight", &jets_isBadTight, &b_jets_isBadTight);
    // ntupVar("jets_m", &jets_m, &b_jets_m);
    // ntupVar("jets_phi", &jets_phi, &b_jets_phi);
    // ntupVar("jets_pt", &jets_pt, &b_jets_pt);
    // ntupVar("jets_signal", &jets_signal, &b_jets_signal);
    // ntupVar("larFlags", &larFlags, &b_larFlags);
    // ntupVar("lumiBlock", &lumiBlock, &b_lumiBlock);
    // ntupVar("lumiFlags", &lumiFlags, &b_lumiFlags);
    ntupVar("muons", signalMuon, true, true, false, true);
    // ntupVar("mu_density", &mu_density, &b_mu_density);
    // ntupVar("muonFlags", &muonFlags, &b_muonFlags);
    // ntupVar("muons_IFFClassType", &muons_IFFClassType, &b_muons_IFFClassType);
    // ntupVar("muons_MT", &muons_MT, &b_muons_MT);
    // ntupVar("muons_charge", &muons_charge, &b_muons_charge);
    // ntupVar("muons_d0sig", &muons_d0sig, &b_muons_d0sig);
    // ntupVar("muons_e", &muons_e, &b_muons_e);
    // ntupVar("muons_eta", &muons_eta, &b_muons_eta);
    // ntupVar("muons_isol", &muons_isol, &b_muons_isol);
    // ntupVar("muons_phi", &muons_phi, &b_muons_phi);
    // ntupVar("muons_pt", &muons_pt, &b_muons_pt);
    // ntupVar("muons_signal", &muons_signal, &b_muons_signal);
    // ntupVar("muons_truthOrigin", &muons_truthOrigin, &b_muons_truthOrigin);
    // ntupVar("muons_truthType", &muons_truthType, &b_muons_truthType);
    // ntupVar("muons_z0sinTheta", &muons_z0sinTheta, &b_muons_z0sinTheta);
    ntupVar("n_BJets", (int)signalBJets.size());
    ntupVar("n_BaseElec", (int)baseEle.size());
    ntupVar("n_BaseJets", (int)baseJets.size());
    ntupVar("n_BaseMuon", (int)baseMuon.size());
    ntupVar("n_BaseTau", (int)baseTaus.size());
    ntupVar("n_SignalElec", (int)signalEle.size());
    ntupVar("n_SignalJets", (int)signalJets.size());
    ntupVar("n_SignalMuon", (int)signalMuon.size());
    ntupVar("n_SignalTau", (int)signalTaus.size());
    // ntupVar("pixelFlags", &pixelFlags, &b_pixelFlags);
    // ntupVar("runNumber", &runNumber, &b_runNumber);
    // ntupVar("sctFlags", &sctFlags, &b_sctFlags);
    ntupVar("taus", signalTaus, true, true, false, true);
    // ntupVar("taus_BDTEleScore", &taus_BDTEleScore, &b_taus_BDTEleScore);
    // ntupVar("taus_BDTEleScoreSigTrans", &taus_BDTEleScoreSigTrans, &b_taus_BDTEleScoreSigTrans);
    // ntupVar("taus_ConeTruthLabelID", &taus_ConeTruthLabelID, &b_taus_ConeTruthLabelID);
    // ntupVar("taus_MT", &taus_MT, &b_taus_MT);
    // ntupVar("taus_NTrks", &taus_NTrks, &b_taus_NTrks);
    // ntupVar("taus_NTrksJet", &taus_NTrksJet, &b_taus_NTrksJet);
    // ntupVar("taus_PartonTruthLabelID", &taus_PartonTruthLabelID, &b_taus_PartonTruthLabelID);
    // ntupVar("taus_Quality", &taus_Quality, &b_taus_Quality);
    // ntupVar("taus_RNNJetScore", &taus_RNNJetScore, &b_taus_RNNJetScore);
    // ntupVar("taus_RNNJetScoreSigTrans", &taus_RNNJetScoreSigTrans, &b_taus_RNNJetScoreSigTrans);
    // ntupVar("taus_TrkJetWidth", &taus_TrkJetWidth, &b_taus_TrkJetWidth);
    // ntupVar("taus_Width", &taus_Width, &b_taus_Width);
    // ntupVar("taus_charge", &taus_charge, &b_taus_charge);
    // ntupVar("taus_d0", &taus_d0, &b_taus_d0);
    // ntupVar("taus_d0sig", &taus_d0sig, &b_taus_d0sig);
    // ntupVar("taus_e", &taus_e, &b_taus_e);
    // ntupVar("taus_eta", &taus_eta, &b_taus_eta);
    // ntupVar("taus_phi", &taus_phi, &b_taus_phi);
    // ntupVar("taus_pt", &taus_pt, &b_taus_pt);
    // ntupVar("taus_signalID", &taus_signalID, &b_taus_signalID);
    // ntupVar("taus_trks_d0", &taus_trks_d0, &b_taus_trks_d0);
    // ntupVar("taus_trks_d0sig", &taus_trks_d0sig, &b_taus_trks_d0sig);
    // ntupVar("taus_trks_z0sinTheta", &taus_trks_z0sinTheta, &b_taus_trks_z0sinTheta);
    // ntupVar("taus_truthOrigin", &taus_truthOrigin, &b_taus_truthOrigin);
    // ntupVar("taus_truthType", &taus_truthType, &b_taus_truthType);
    // ntupVar("taus_z0sinTheta", &taus_z0sinTheta, &b_taus_z0sinTheta);
    // ntupVar("tileFlags", &tileFlags, &b_tileFlags);
    // ntupVar("trtFlags", &trtFlags, &b_trtFlags);

    accept("preselection");

    return;
}
