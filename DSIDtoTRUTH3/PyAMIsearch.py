#############################################################################
# IMPORT PYAMI CLIENT MODULE AND API STATIC FUNCTION                        #
#############################################################################
import pyAMI.client
import pyAMI.atlas.api as api
import json
import re


#############################################################################
# INIT ATLAS API                                                            #
#############################################################################
#api.init()
#[`ami_status`, `ami_tags`, `atlas_release`, `beam`, `completion`, `conditions_tag`, `cross_section`, `dataset_number`, `ecm_energy`, `events`, `generator_filter_efficienty`, `generator_name`, `generator_tune`, `geometry`, `in_container`, `job_config`, `ldn`, `modified`, `nfiles`, `pdf`, `period`, `physics_comment`, `physics_short`, `prodsys_status`, `production_step`, `project`, `requested_by`, `responsible`, `run_number`, `stream`, `total_size`, `transformation_package`, `trash_annotation`, `trash_date`, `trash_trigger`, `trigger_config`, `type`]
#############################################################################
# INSTANTIATE THE PYAMI CLIENT FOR ATLAS                                    #
#############################################################################
class PyAMIsearch():

    def __init__(self):
        self.client = pyAMI.client.Client(['atlas'])
#############################################################################
# USE ATLAS API (http://ami.in2p3.fr/pyAMI/pyAMI5_atlas_api.html)           #
# https://ami.in2p3.fr/pyAMI-4.1.3/reference/api.html                       #
# Need filling fields because they are what api will return                 #
#############################################################################

    def searchDSID(self, number, re_patterns=[]):

        patterns = number
        fields = 'nfiles,events,generator_name,dataset_number,type'
        limit = 100
        target_type = "DAOD_PHYS"

        resDict = api.list_datasets(self.client, dataset_number=patterns, fields=fields, limit=limit, type=target_type)

        #print(resDict)
        ever_find_TRUTH3 = 0
        not_found_pat = ""

        return_one_line = ''
        for resItem in resDict:
            ever_find_TRUTH3 = 1
            for pat in re_patterns:
                re_pat = re.compile(pat)
                DSID_name = resItem['ldn']
                if not self.addTagFilter(re_pat, DSID_name):
                    not_found_pat = pat
                    #print(pat, DSID_name)
                    ever_find_TRUTH3 = 0
                    break
            if ever_find_TRUTH3 == 1:
                return_one_line = return_one_line + resItem['ldn'] + " , "

        if return_one_line:
            return return_one_line
        else:
            if not_found_pat:
                return_one_line = "DSID: " + number + ' , ' + not_found_pat + ' not found'
            else:
                return_one_line = "DSID: " + number + ' , ' + target_type + ' not found'
            for resItem in resDict:
                if resItem['type'] == 'EVNT':
                    return_one_line = return_one_line + " , " + resItem['ldn']
            return return_one_line

    def addTagFilter(self, tag_pattern, data_name):
        if re.search(tag_pattern, data_name):
            return 1
        else:
            return 0


#############################################################################
# PRINT PYTHON DICT AS JSON FORMAT                                          #
#############################################################################
#print(json.dumps(resDict,indent=4))
