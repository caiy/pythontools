import re
import PyAMIsearch
import argparse

parser = argparse.ArgumentParser(description='input DSID and search the TRUTH3')
parser.add_argument('-i', '--input', action='store', help='input txt file')
parser.add_argument('-o', '--output', action='store', help='output file')
args = vars(parser.parse_args())

if __name__ == "__main__":

    AMI = PyAMIsearch.PyAMIsearch()

    in_file_object = open(args['input'], "r")
    out_file_object = open(args['output'], "w+")
    #logging.basicConfig(filename='example.log',level=logging.DEBUG)
    line_contents = in_file_object.readlines()

    in_file_object.close()

    for each_line in line_contents:
        if re.match(r'[A-Za-z]+|$', each_line) and not re.match(r'^\s*$', each_line):
            DSID_pat = r'[0-9]{6}'
            rDSID = re.search(DSID_pat, each_line)
            RecoTag_pat = r'r9[0-9]{3}|r1[0-9]{4}'
            rRecoTag = re.search(RecoTag_pat, each_line)
            #print(rRecoTag.group(0))
            AMI_return = AMI.searchDSID(rDSID.group(0), [rRecoTag.group(0)])  #"p4164|p4178|p4355|p4320",, ["r10201"]
            out_file_object.write(AMI_return + "\n")
            #logging.info(AMI_return)
        else:
            out_file_object.write(each_line)
            #logging.info(each_line)
        #print(type(each_line))
    out_file_object.close()
