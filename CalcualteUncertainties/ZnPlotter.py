# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import csv, json, math
from scipy.interpolate import griddata, SmoothBivariateSpline
import restored_points

global_label = r''
global_pre_region_name = ""
global_in_json_name = ""
global_var_name = ""
global_region_name = ""


def global_config(label, pre_region_name, in_json_name, var_name, region_name):
    global global_label, global_pre_region_name, global_out_image_name, global_in_json_name, global_var_name, global_region_name
    global_label = label
    global_pre_region_name = pre_region_name
    global_in_json_name = in_json_name
    global_var_name = var_name
    global_region_name = region_name
    return 1


def indices(lst, element):
    result = []
    offset = -1
    while True:
        try:
            offset = lst.index(element, offset + 1)
        except ValueError:
            return result
        result.append(offset)


def find_point(list_x, px, list_y, py):
    indices_x = indices(list_x, px)
    indices_y = indices(list_y, py)
    return list(set(indices_x).intersection(indices_y))


def cal_var(nom, nom_pre, var, var_pre):
    if nom == 0 or nom_pre == 0 or var_pre == 0:
        return 0
    return abs((nom / nom_pre - var / var_pre) / (nom / nom_pre))
    #return abs((nom-var)/nom)


def calcZn(bkg, bkgerr2, sig):
    if bkg <= 0:
        return 0
    else:
        n = bkg + sig
        dll = n * np.log(( n * (bkg + bkgerr2)) / (bkg * bkg + n * bkgerr2)) - \
         ((bkg * bkg) / (bkgerr2)) * np.log((bkg * bkg + n * bkgerr2) / (bkg * (bkg + bkgerr2)))
        nsigma = np.sqrt(2 * dll)
        return nsigma


def str_to_position(in_str):
    in_str = in_str.replace("p", ".")
    position_x_y = in_str.split("_")
    px = float(position_x_y[0])
    py = float(position_x_y[1])
    return [px, py]


def read_json(json_path, var_name, ragion_name):
    xx = []
    yy = []
    zz = []
    with open(json_path) as infile:
        json_file = json.load(infile)
    for each_point in json_file:
        if var_name in json_file[each_point]:
            xx.append(str_to_position(str(each_point))[0])
            yy.append(str_to_position(str(each_point))[1])
            zz.append(
                cal_var(json_file[each_point]["nom"][ragion_name], json_file[each_point]["nom"][global_pre_region_name],
                        json_file[each_point][var_name][ragion_name], json_file[each_point][var_name][global_pre_region_name]))
    return xx, yy, zz


def add_boundary_condition(xx, yy, zz):
    negative_zz = [-1 * i for i in zz]
    return xx + yy, yy + xx, zz + negative_zz


def main():
    xx, yy, zz = read_json(global_in_json_name, global_var_name, global_region_name)
    xxx = []
    yyy = []

    xx, yy, zz = add_boundary_condition(xx, yy, zz)

    xx = np.asarray(xx)
    yy = np.asarray(yy)
    zz = np.asarray(zz)

    x = np.linspace(0, 1600, 1601)
    y = np.linspace(0, 900, 901)
    X, Y = np.meshgrid(x, y)
    #z0=griddata(points=(xx,yy),values=zz,xi=(X,Y),method='nearest')
    z1 = griddata(points=(xx, yy), values=zz, xi=(X, Y), method='cubic')
    for i in range(len(y)):
        for j in range(0, i):
            z1[i][j] = float('nan')
        for j in range(i, len(x)):
            if z1[i][j] >= 1:
                z1[i][j] = 0.999
            if z1[i][j] <= 0:
                z1[i][j] = 0.0001
    return x, y, z1, xx, yy, zz


def draw(x, y, z, xx=[], yy=[], zz=[], out_image_name=""):
    line = np.linspace(0, 700, 70)
    xAxis = r'$m{\tilde{\chi}^{\pm}_1}$ / $m{\tilde{\chi}^{0}_2}$ [GeV]'
    #xAxis=r'$m{\tilde{\chi}^{\pm}_1}$ [GeV]'
    yAxis = r'$m{\tilde{\chi}^1_0}$ [GeV]'

    clev = np.arange(0., 1.1, .1)
    #color bar
    fig = plt.contourf(x, y, z, levels=clev, vmin=0., vmax=1., cmap=cm.coolwarm)
    m = plt.cm.ScalarMappable(cmap=cm.coolwarm)
    m.set_array(z)
    m.set_clim(0., 1.)
    cbar = plt.colorbar(m)
    cbar.set_label(r'$\sigma$')

    #grid, forbidden line and text
    plt.grid(color='grey', linestyle='--', linewidth=0.5)
    plt.plot(line, line, color='grey', linestyle='dashed')
    plt.text(200, 300, r'$m{\tilde{\chi}^{\pm}_1\ /\ \tilde{\chi}^{0}_2} > m{\tilde{\chi}^1_0}$', rotation=63, color='grey', fontsize=12)
    if len(xx) != 0:
        for i in range(len(xx)):
            if xx[i] > yy[i]:
                plt.text(xx[i], yy[i] + 4, str(round(100 * zz[i], 0)), rotation=20, color='black', fontsize=4)

    #ATLAS Internal, lumi, cmEnergy
    plt.text(40, 810, r'ATLAS', fontsize=12, weight="bold", style="italic")
    plt.text(300, 810, r'Internal', fontsize=12)
    plt.text(40, 760, r'$\sqrt{s}=13\ TeV,\ 139\ fb^{-1}$', fontsize=8)

    #plot Right head label and axix
    plt.text(590, 810, global_label, fontsize=8)

    #
    plt.xlabel(xAxis)
    plt.ylabel(yAxis)

    plt.savefig(out_image_name, dpi=200)
    plt.clf()


if __name__ == "__main__":

    region = 'SR4'
    grid = "C1N2"
    pre = "Pre_34"

    out_dict = {}
    variations = ["py1up", "py1dw", "py2up", "py2dw", "py3aup", "py3adw", "py3bup", "py3bdw", "py3cup", "py3cdw", "qcup", "qcdw", "scup", "scdw"]
    square_raup = ["py1up", "py2up", "py3aup", "py3bup", "py3cup"]
    square_radw = ["py1dw", "py2dw", "py3adw", "py3bdw", "py3cdw"]
    raup_z = []
    radw_z = []

    outjson = grid + "_" + region + ".json"

    for var in variations:
        outimage = grid + "_" + region + "_" + var + ".png"
        injson = "./" + grid + ".json"

        if global_config(label=region, pre_region_name=pre, in_json_name=injson, var_name=var, region_name=region):

            x, y, z, xx, yy, zz = main()

            if var in square_raup:
                raup_z.append(z)
            elif var in square_radw:
                radw_z.append(z)

            draw(x, y, z, xx, yy, zz, outimage)

            list_x = x.tolist()
            list_y = y.tolist()

            out_sub_dict = {}
            for each_point in restored_points.restored_points:
                nx = list_x.index(each_point[0])
                ny = list_y.index(each_point[1])
                out_sub_dict[str(each_point[0]) + "_" + str(each_point[1])] = round(abs(z[ny][nx]), 3)
            #print(out_sub_dict)
        out_dict[var] = out_sub_dict

    print(len(raup_z), len(radw_z))
    total_raup_z = np.sqrt(
        np.multiply(raup_z[0], raup_z[0]) + np.multiply(raup_z[1], raup_z[1]) + np.multiply(raup_z[2], raup_z[2]) +
        np.multiply(raup_z[3], raup_z[3]) + np.multiply(raup_z[4], raup_z[4]))
    total_radw_z = np.sqrt(
        np.multiply(radw_z[0], radw_z[0]) + np.multiply(radw_z[1], radw_z[1]) + np.multiply(radw_z[2], radw_z[2]) +
        np.multiply(radw_z[3], radw_z[3]) + np.multiply(radw_z[4], radw_z[4]))

    for i in range(len(y)):
        for j in range(i, len(x)):
            if total_raup_z[i][j] >= 1:
                total_raup_z[i][j] = 0.999
            if total_raup_z[i][j] <= 0:
                total_raup_z[i][j] = 0.0001
            if total_radw_z[i][j] >= 1:
                total_radw_z[i][j] = 0.999
            if total_radw_z[i][j] <= 0:
                total_radw_z[i][j] = 0.0001

    xx = []
    yy = []
    zz = []
    list_x = x.tolist()
    list_y = y.tolist()
    out_sub_dict = {}
    for each_point in restored_points.restored_points:
        nx = list_x.index(each_point[0])
        ny = list_y.index(each_point[1])
        if total_raup_z[ny][nx] >= 1:
            total_raup_z[ny][nx] = 1
        out_sub_dict[str(each_point[0]) + "_" + str(each_point[1])] = round(abs(total_raup_z[ny][nx]), 3)
        xx.append(each_point[0])
        yy.append(each_point[1])
        zz.append(total_raup_z[ny][nx])
    draw(x, y, total_raup_z, xx, yy, zz, out_image_name=grid + "_" + region + "_" + "raup" + ".png")
    out_dict["raup"] = out_sub_dict

    xx = []
    yy = []
    zz = []
    list_x = x.tolist()
    list_y = y.tolist()
    out_sub_dict = {}
    for each_point in restored_points.restored_points:
        nx = list_x.index(each_point[0])
        ny = list_y.index(each_point[1])
        if total_radw_z[ny][nx] >= 1:
            total_radw_z[ny][nx] = 1
        out_sub_dict[str(each_point[0]) + "_" + str(each_point[1])] = round(abs(total_radw_z[ny][nx]), 3)
        xx.append(each_point[0])
        yy.append(each_point[1])
        zz.append(total_radw_z[ny][nx])
    draw(x, y, total_radw_z, xx, yy, zz, out_image_name=grid + "_" + region + "_" + "radw" + ".png")
    out_dict["radw"] = out_sub_dict

    print(out_dict)
    with open(outjson, "w") as json_file:
        json.dump(out_dict, json_file, indent=4)
