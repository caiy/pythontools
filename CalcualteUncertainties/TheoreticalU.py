# -*- coding: utf-8 -*-

from __future__ import print_function
import csv
import os, fnmatch
import math
import re
import json
"""
         0         1         2         3         4
T = | Nom_nom | Nom_sig | Sys_nom | Sys_sig | Result |
    |         |         |         |         |        |
"""


def find_file(path, pattern, filetype):
    result = []
    pattern = "*" + str(pattern) + "*" + filetype
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


def read_file(file_path):

    with open(file_path, mode='r') as csv_file:
        csv_reader = csv.reader(csv_file)
        csv_to_dict = dict((row[0], float(row[1].split("+-")[0])) for row in csv_reader)
    return csv_to_dict


if __name__ == "__main__":
    summary_dict = {}
    all_files = find_file("/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/SignalTheoreticalUncer/RegionSets/run/", 'C1N2', '.csv')
    #all_files = find_file( "/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/SignalTheoreticalUncer/RegionSets/run/" , 'C1C1' , '.csv')
    print(all_files)

    for each_file in all_files:

        csv_dict = read_file(each_file)

        position = re.findall(r"\..+\.", each_file)[0]
        position = position[1:-1]
        position_split = position.split("_")

        if len(position_split) == 2:
            position_split.append("nom")
        if position_split[0] + '_' + position_split[1] in summary_dict:
            temp_dict = summary_dict[position_split[0] + '_' + position_split[1]]
            temp_dict[position_split[2]] = csv_dict
            summary_dict[position_split[0] + '_' + position_split[1]] = temp_dict
        else:
            temp_dict = {position_split[2]: csv_dict}
            summary_dict[position_split[0] + '_' + position_split[1]] = temp_dict
        print(position)

    all_files = find_file("/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/SignalTheoreticalUncer/RegionSets/run/", 'C1C1', '.csv')
    print(all_files)

    for each_file in all_files:

        csv_dict = read_file(each_file)

        position = re.findall(r"\..+\.", each_file)[0]
        position = position[1:-1]
        position_split = position.split("_")

        if len(position_split) == 2:
            position_split.append("nom")

        for var in summary_dict[position_split[0] + '_' + position_split[1]][position_split[2]]:
            summary_dict[position_split[0] + '_' + position_split[1]][position_split[2]][var] += csv_dict[var]

    with open("Combine.json", "w") as json_file:
        json.dump(summary_dict, json_file, indent=4)
    #print(summary_dict)
    #print( read_file("/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/SignalTheoreticalUncer/RegionSets/run/C1C1.500p0_300p0_qcdw.csv") )
