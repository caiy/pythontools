# -*- coding: utf-8 -*-

from __future__ import print_function
import csv
import os, fnmatch


def find_file(path, pattern, filetype):
    result = ""
    pattern = "*" + str(pattern) + "*" + filetype
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result = os.path.join(root, name)
    return result


def addNominal(runpath, Region, DSIDlist, Darray):
    for i in DSIDlist:
        Sfile = find_file(runpath, i, 'csv')
        print(i, ": ", Sfile)
        try:
            with open(Sfile) as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                for row in readCSV:
                    for rowN in range(len(Region)):
                        if Region[rowN] == row[0]:
                            Darray[rowN][0] += float(row[1].split("+-")[0])
                            break
        except IOError:
            print(i, " this nominal file is empty or not exists, so set to ZERO")
    return Darray


def addVariation(runpath, Region, DSIDlist, Darray):
    for i in DSIDlist:
        Sfile = find_file(runpath, i, 'csv')
        print(i, ": ", Sfile)
        try:
            with open(Sfile) as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                for row in readCSV:
                    for rowN in range(len(Region)):
                        if Region[rowN] == row[0]:
                            Darray[rowN][1] += float(row[1].split("+-")[0])
                            break
        except IOError:
            print(i, " this variation file is empty or not exists, so set to ZERO")
    return Darray


def main():

    runpath = './RegionSets/run/'
    Region = ["SR1", "SR2", "SR3", "SR4", "WCR", "Pre"]

    Mb_Nominal = [345705, 345706, 345723, 364250, 364253, 364254, 364283, 364284, 364286, 364287, 364285]
    Mb_CKKW30 = [345849, 364371, 364257, 366038, 364276, 366043, 364298, 366063, 364366, 366068, 366048]
    Mb_CKKW15 = [345848, 364372, 364256, 366039, 364275, 366044, 364297, 366064, 364367, 366069, 366049]
    Mb_CSSKIN = [345852, 364370, 364260, 366040, 364279, 366045, 364299, 366065, 364365, 366070, 366050]
    Mb_QSF025 = [345850, 366037, 364258, 366042, 364277, 366062, 364369, 366067, 364374, 364300, 366047]
    Mb_QSF4 = [345851, 366036, 364259, 366041, 364278, 366061, 364368, 366066, 364373, 364301, 366046]

    #ttbar  SingleTop  ttV#
    tNominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]

    ttbar_HardScat_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219,
                              410220]  # Missing mc15_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad
    ttbar_HardScat_Var = [410464, 410465, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]

    ttbar_PartonShower_Nominal = [410470]  # Missing mc15_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad
    ttbar_PartonShower_Var = [410557,
                              410558]  #something wrong with mc15_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.evgen.EVNT.e6366
    #,  410644,410645,410646,410647,410658,410659,  410155,410156,410157,410218,410219,410220
    #,  410644,410645,410646,410647,410658,410659,  410155,410156,410157,410218,410219,410220
    #ttbar_ISRUp_Nominal = [410470,  410644,410645,410646,410647,410658,410659,  410155,410156,410157,410218,410219,410220]
    #ttbar_ISRUp_Var = [410480,410482,  410644,410645,410646,410647,410658,410659,  410155,410156,410157,410218,410219,410220] # Missing mc15_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad

    singleTop_HardScat_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]
    singleTop_HardScat_Var = [410470, 412005, 412002, 412004, 410155, 410156, 410157, 410218, 410219, 410220]

    singleTop_PartonShower_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]
    singleTop_PartonShower_Var = [410470, 411034, 411035, 411032, 411033, 411036, 411037, 410155, 410156, 410157, 410218, 410219,
                                  410220]  # PowhegHerwig7  ,411038,411039
    #
    #

    singleTop_WtttbarInter_Nominal = [410470, 410646, 410647, 410644, 410645, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]
    singleTop_WtttbarInter_Var = [410470, 410654, 410655, 410644, 410645, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]

    ttV_PartonShower_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]
    ttV_PartonShower_Var = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410158, 410157, 412090, 412091, 412092]  # PowhegHerwig7

    ttV_ISRUp_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]
    ttV_ISRUp_Var = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410376, 410378, 410380, 410370, 410372, 410374]

    ttV_ISRDown_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]  #
    ttV_ISRDown_Var = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410377, 410379, 410381, 410371, 410373, 410375]  #

    ttV_HardScat_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]
    ttV_HardScat_Var = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 413022]

    higgs_PartonShower_Nominal = [346343, 346344, 346345]
    higgs_PartonShower_Var = [346346, 346347, 346348]
    #tNominal = [410470, 410644,410646,410647,410658,410659, 410155,410156,410157,410218,410219,410220]
    #tHardScatter = [410464,410465, 412004,412002, 410155,410156,410157,413022]
    #tPartonShower = [410557,410558, 411032,411034,411037, 410155,410156,410157,410218,410219,410220]
    #tNominal = [410470, 410644,410645,410646,410647,410658,410659, 410155,410156,410157,410218,410219,410220]
    Top_PartonShower_Nominal = [410470, 410644, 410645, 410646, 410647, 410658, 410659, 410155, 410156, 410157, 410218, 410219, 410220]  #
    Top_PartonShower_Var = [410557, 410558, 411034, 411035, 411032, 411033, 411036, 411037, 410155, 410158, 410157, 412090, 412091, 412092]  #

    T = [[0, 0, 0] for i in range(len(Region))]
    T = addNominal(runpath, Region, Top_PartonShower_Nominal, T)
    T = addVariation(runpath, Region, Top_PartonShower_Var, T)
    #Mb_CKKW15
    print("Region", "Delta", "Nominal", "Variation")
    for i in range(len(T)):
        try:
            T[i][2] = abs((T[i][0] - T[i][1]) / T[i][0])
        except ZeroDivisionError:
            print(Region[i], " Nominal = 0, set variation to 0")
            T[i][2] = 0
        #print(Region[i],T[i][2],T[i][0],T[i][1])
        print(T[i][2])
    #print(T)


if __name__ == "__main__":
    main()
