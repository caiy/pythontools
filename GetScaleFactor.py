import csv
import numpy as np
import sys

file1 = "/publicfs/atlas/atlasnew/SUSY/users/caiyc/R21_tau/Analysis/PrePlot/run/yields_WCR_os_1p.csv"


def plus_minus(str_add, str_sub=[], scale_factor=1):
    new_value = 0.000
    new_error = 0.000
    for str_each in str_add:
        str_split = str_each.split('+-')
        new_value = new_value + round(scale_factor * float(str_split[0]), 2)
        new_error = round(
            np.sqrt(new_error * new_error + round(scale_factor * float(str_split[1]), 2) * scale_factor * round(float(str_split[1]), 2)), 2)
    for str_each in str_sub:
        str_split = str_each.split('+-')
        new_value = new_value - round(scale_factor * float(str_split[0]), 2)
        new_error = round(
            np.sqrt(new_error * new_error + round(scale_factor * float(str_split[1]), 2) * scale_factor * round(float(str_split[1]), 2)), 2)
    new_str = str(new_value) + '+-' + str(new_error)
    return new_value, new_error, new_str


def print_plus_minus(input_dic):
    print('Data', plus_minus([input_dic['Data.A.root'], input_dic['Data.D.root'], input_dic['Data.E.root']])[2])
    print('TTbar', plus_minus([input_dic['TTbar.A.root'], input_dic['TTbar.D.root'], input_dic['TTbar.E.root']])[2])
    print('Zll', plus_minus([input_dic['Zll.A.root'], input_dic['Zll.D.root'], input_dic['Zll.E.root']])[2])
    print('Ztt', plus_minus([input_dic['Ztt.root']])[2])
    print('W', plus_minus([input_dic['W.root']])[2])
    print('SingleTop', plus_minus([input_dic['SingleTop.root']])[2])


with open(file1, mode='r') as WCR_os_1p:
    reader_WCR_os_1p = csv.reader(WCR_os_1p)
    WCR_os_1p_dict = dict((row[0], row[1]) for row in reader_WCR_os_1p)
    print(WCR_os_1p_dict)
    print_plus_minus(WCR_os_1p_dict)
